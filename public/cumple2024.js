$(window).on("load", () => {
    // flip the card to the back and change UI
    $("#btn-card-flip").click(function (e) {
        $(".card").addClass("flip");
        e.stopPropagation();
    });

    // flip the card to the front and change UI
    $("#close").click(function (e) {
        // 'close' button is pressed
        $(".card").removeClass("flip");
        e.stopPropagation();
    });

    // don't allow links to flip the card
    $(".back-option a").click(function (e) {
        // Do something
        e.stopPropagation();
    });

    // intro animation
    $("#body").ready(function () {
        $(".card").addClass("whoosh");
    });
});